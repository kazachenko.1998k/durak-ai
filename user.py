import math
import random

import const
import numpy as np
from enum import Enum


class StepResult(Enum):
    PASS = 0,
    GET = 1,
    ADD = 2,
    ANSWER = 3,
    NONE = -1


def game():
    stack = list(range(0, const.all_cards))
    random.shuffle(stack)
    hack = stack[0]
    users = []
    for i in range(0, const.users_count):
        users.append([])
    index = 0
    take_from_stack(users, stack)
    table = []
    last_step = StepResult.NONE
    while not has_win(users, stack):
        last_result = step(users[index], table, hack, index, last_step)
        if last_result != StepResult.NONE:
            last_step = last_result
            index = (index + 1) % const.users_count
            print(last_step, human_log(users[0]), human_log(users[1]), human_log(table))
        if last_result == StepResult.GET or last_result == StepResult.PASS:
            take_from_stack(users, stack)
    print(users)
    print(stack)


def take_from_stack(users, stack):
    for index in range(0, const.users_count):
        user_hand = users[index]
        while 0 < len(stack) and len(user_hand) < const.cards_on_hand:
            user_hand.append(stack.pop())


def has_win(users, stack):
    if len(stack) > 0:
        return False
    for hand in users:
        if len(hand) == 0:
            return True
    return False


def step(hand, table, hack, is_my_turn, last_result):
    hand_array = np.zeros((const.all_cards,), dtype=int)
    for index in hand:
        hand_array[index] = 1
    table_array = np.zeros((const.cards_on_table_max,), dtype=int)
    for index, value in enumerate(table):
        table_array[index] = value
    input_array = np.concatenate((hand_array, table_array, [hack], [is_my_turn], [last_result]))
    result_action = proceedAction(input_array)
    result = analyse_result(result_action, hand, table, hack, last_result == StepResult.ADD)
    return result


def analyse_result(result_action, hand, table, hack, i_need_to_play):
    if i_need_to_play:  # i am defend
        if result_action in hand and \
                len(table) < const.cards_on_table_max and \
                card_can_answer_to_table(table, result_action, hack):  # answer from hand
            hand.remove(result_action)
            table.append(result_action)
            return StepResult.ANSWER
        if result_action < 0 and len(table) > 0:  # get all
            for elem in table:
                hand.append(elem)
            table.clear()
            return StepResult.GET
    else:  # i am attack
        if result_action < 0 and len(table) > 0:  # pass
            table.clear()
            return StepResult.PASS
        elif result_action in hand and \
                len(table) < const.cards_on_table_max and \
                (len(table) == 0 or card_can_add_to_table(table, result_action)):
            hand.remove(result_action)
            table.append(result_action)
            return StepResult.ADD
    return StepResult.NONE


def proceedAction(input):
    return random.randint(-1, const.all_cards)


def card_can_add_to_table(table, card):
    for card_in_table in table:
        if card_in_table % const.cards_on_suit == card % const.cards_on_suit:
            return True
    return False


def card_can_answer_to_table(table, card, hack):
    fight_card = table[-1]
    if math.floor(fight_card / const.cards_on_suit) == math.floor(card / const.cards_on_suit):
        return card > fight_card
    if card % const.cards_on_suit == hack % const.cards_on_suit:
        return True
    return False


def human_log(cards):
    result_str = []
    for card in cards:
        value = card % const.cards_on_suit + 6
        if value == 11:
            value = 'B'
        if value == 12:
            value = 'Q'
        if value == 13:
            value = 'K'
        if value == 14:
            value = 'T'
        suit = math.floor(card / const.cards_on_suit)
        if suit == 0:
            suit = 'чер'
        if suit == 1:
            suit = 'буб'
        if suit == 2:
            suit = 'пик'
        if suit == 3:
            suit = 'кре'
        result_str.append("{}{}".format(value, suit))
    return result_str


game()
